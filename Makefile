USERNAME=rttov

build:
	docker build -t rttovgui:1 .

run: build
	XAUTH=/tmp/.docker.xauth ; rm -f $$XAUTH ;\
    xauth nlist $$DISPLAY | sed -e 's/^..../ffff/' | xauth -f $$XAUTH nmerge - ;\
    chmod 777 $$XAUTH ; \
    docker run -it -e DISPLAY=$$DISPLAY -v $$XAUTH:$$XAUTH -e XAUTHORITY=$$XAUTH \
    --net=host \
	-v ${HOME}/.rttov:/home/${USERNAME}/.rttov:rw \
	-v ${RTTOV_FILES}/:/home/${USERNAME}/RTTOV_FILES \
    -h rttovgui rttovgui:1

bash: build
	XAUTH=/tmp/.docker.xauth ; rm -f $$XAUTH ;\
    xauth nlist $$DISPLAY | sed -e 's/^..../ffff/' | xauth -f $$XAUTH nmerge - ;\
    chmod 777 $$XAUTH ; \
    docker run -it -e DISPLAY=$$DISPLAY -v $$XAUTH:$$XAUTH -e XAUTHORITY=$$XAUTH \
    --net=host \
	-v ${HOME}/.rttov:/home/${USERNAME}/.rttov:rw \
	-v ${RTTOV_FILES}/:/home/${USERNAME}/RTTOV_FILES \
    -h rttovgui rttovgui:1 -c /bin/sh

rootbash: build
	XAUTH=/tmp/.docker.xauth ; rm -f $$XAUTH ;\
    xauth nlist $$DISPLAY | sed -e 's/^..../ffff/' | xauth -f $$XAUTH nmerge - ;\
    chmod 777 $$XAUTH ; \
    docker run -it --user 0 -e DISPLAY=$$DISPLAY -v $$XAUTH:$$XAUTH -e XAUTHORITY=$$XAUTH \
    --net=host \
	-v ${HOME}/.rttov:/home/${USERNAME}/.rttov:rw \
	-v ${RTTOV_FILES}/:/home/${USERNAME}/RTTOV_FILES \
    -h rttovgui rttovgui:1 -c /bin/sh

