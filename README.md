# rttovgui docker

**Dockerfile for RTTOV GUI**

This project contains: 
- A Dockerfile
- A Makefile which contains the Docker command to build the Docker image and run the Docker container.
- additional files

## Docker image construction and container usage:

### prerequisites:
Before to be able to launch the container, additionnal files are needed:
The source code of RTTOV, RTTOV coefficient files and RTTOV atlases.
The source code of RTTOV is needed for the creation of the Docker image
The RTTOV coefficient files and atlases are needed when the Docker container is launched.

Download RTTOV tar file from the NWPSAF website
The tar files must be put in the tarfiles directory.
expected syntax : 
rttov<version>.tar.xz

Modify RTTOV_VERSION in Dockerfile with the version number

### Download rttov files (coefficient files and atlases) in a dedicated directory on your computer

**coefficient files:**

Extract the rtcoef_rttov13 of the tar file in a dedicated directory:

`tar xf <path_to_tarfiles>/rttov<version>.tar.xz rtcoef_rttov13`

`cd rtcoef_rttov13`

`./rttov_coef_download.sh`

**Atlases:**
The atlases can be downloaded with the script "scripts/download_atlases.sh" 
note that the  download is performed in the current directory.


Define the environemnt variable **RTTOV_FILES** to point to the directory where you have downloaded the RTTOV coefficient files and atlases.
This directory must contain these 3 sub-directories:
- rtcoef_rttov13
- emis_data
- brdf_data

Create a directory named .rttov on your $HOME directory and set the rights to rwx for everyone:

`mkdir -p ~/.rttov`

`chmod 777 ~/.rttov`

### Docker image build:

`make`

### Docker container launch:

`make run`

