#!/bin/sh
# script to download emis and brdf atlases for RTTOV
mkdir -p emis_data
cd emis_data
url_download="https://nwp-saf.eumetsat.int/downloads/emis_data/"
for file in uw_ir_emis_atlas_hdf5.tar \
            camel_clim_emis_atlas_pchsr.tar\
            camel_clim_emis_atlas_jan-mar.tar\
            camel_clim_emis_atlas_apr-jun.tar\
            camel_clim_emis_atlas_jul-sep.tar\
            camel_clim_emis_atlas_oct-dec.tar\
            telsem2_mw_atlas.tar.bz2\
            cnrm_mwemis_amsu_mhs_data.tar\
            cnrm_mwemis_atms_data.tar\
            cnrm_mwemis_ssmis_data.tar
do
echo "download ${url_download}/$file"
  wget ${url_download}/$file
  tar xf $file
  rm $file
done

mkdir -p brdf_data
cd brdf_data
url_download="https://nwp-saf.eumetsat.int/downloads/brdf_data/"
for file in cms_brdf_atlas_hdf5.tar
do
echo "download ${url_download}/$file"
  wget ${url_download}/$file
  tar xf $file
  rm $file
done
echo "Download complete"
exit 0
