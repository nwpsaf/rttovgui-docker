FROM centos:7
MAINTAINER PR "jean-marie.lalande@meteo.fr"
LABEL description="Docker image for RTTOV-GUI"
ENV USERNAME rttov
ENV RTTOV_VERSION 132
ENV HDF5_BASE=1.10
ENV HDF5_RELEASE=1
ENV HDF5_VERSION=${HDF5_BASE}.${HDF5_RELEASE}

RUN yum install -y epel-release \
  gcc-gfortran gcc-c++ make \
  perl perl-Data-Dumper \
  lftp wget bzip2 \
  libpng libpng-devel which \
  zlib-devel libcurl-devel

RUN yum install -y xauth pluma 

RUN yum install -y gstreamer gstreamer-plugins-base\
  gstreamer-plugins-bad gstreamer-plugins-ugly 

# create user 

RUN groupadd -r ${USERNAME} && useradd -r -g ${USERNAME} ${USERNAME}
RUN mkdir /home/${USERNAME} && chown ${USERNAME}:${USERNAME} /home/${USERNAME}
USER ${USERNAME}:${USERNAME}
WORKDIR /home/${USERNAME}

# install miniconda 
COPY --chown=${USERNAME}:${USERNAME} docker_additional_files/rttovguienv.yml /home/${USERNAME}
RUN wget --quiet https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh  &&\
  bash /home/${USERNAME}/Miniconda3-latest-Linux-x86_64.sh -bf -p /home/${USERNAME}/miniconda3 &&\ 
  /home/${USERNAME}/miniconda3/bin/conda init &&\ 
  . /home/${USERNAME}/.bashrc &&\
  /home/${USERNAME}/miniconda3/bin/conda env create -f /home/${USERNAME}/rttovguienv.yml &&\
  conda clean --tarballs  &&\
  conda clean --packages &&\
  echo "conda activate rttovgui" >> /home/${USERNAME}/.bashrc

# get an build hdf5

RUN mkdir -p install build rttov &&\
  cd build &&\
  wget  https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-${HDF5_BASE}/hdf5-${HDF5_VERSION}/src/hdf5-${HDF5_VERSION}.tar.gz &&\
  tar xf hdf5-${HDF5_VERSION}.tar.gz &&\
  cd hdf5-${HDF5_VERSION} &&\
  export CFLAGS=-fPIC &&\
  export FCFLAGS=-fPIC &&\
  ./configure --prefix=/home/${USERNAME}/install/hdf5 --with-pic=PIC --enable-shared=no --with-zlib=yes --enable-fortran=yes --enable-fortran2003=yes &&\
  make install &&\
  make clean &&\
  cd .. && rm -r -f build

   
# get and build rttov

COPY --chown=${USERNAME}:${USERNAME} tarfiles/rttov${RTTOV_VERSION}.tar.xz \
      docker_additional_files/Makefile.local \
      docker_additional_files/rttov_gui.env rttov/
RUN . ./.bashrc && cd rttov && tar xf ./rttov${RTTOV_VERSION}.tar.xz && rm rttov${RTTOV_VERSION}.tar.xz &&\
  mv Makefile.local build && mv rttov_gui.env gui && cd src &&\
  ../build/Makefile.PL RTTOV_HDF=1 RTTOV_F2PY=1 &&\
  make ARCH=gfortran all &&\
  rm -r -f ../tmp-gfortran &&\
  echo ".  /home/${USERNAME}/rttov/gui/rttov_gui.env" >> /home/${USERNAME}/.bashrc &&\
  cd /home/${USERNAME}/rttov/ && rm -r -f src &&\
  mv rtcoef_rttov13 rtcoef_rttov13.orig && mv emis_data emis_data.orig && mv brdf_data brdf_data.orig &&\
  ln -s /home/${USERNAME}/RTTOV_FILES/emis_data &&\
  ln -s /home/${USERNAME}/RTTOV_FILES/brdf_data && ln -s /home/${USERNAME}/RTTOV_FILES/rtcoef_rttov13

RUN  . ./.bashrc && cd rttov/rttov_test/profile-datasets && ./run_convert_test2python.sh &&\
   ./run_convert_python2hdf5.sh

COPY --chown=${USERNAME}:${USERNAME} docker_additional_files/run_gui.sh /

ENTRYPOINT ["/bin/bash"] 
CMD ["/run_gui.sh"]
